import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';

export const SEARCH_INPUT_PARAM = 'searchInput';
export const SEARCH_DEPTH = 'searchDepth';

@Component({
  selector: 'app-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.scss']
})
export class SearchHeaderComponent implements OnInit {
  public searchInputControl = new FormControl('');

  public depthRange = [1, 2, 3, 4, 5];
  public depthControl = new FormControl(this.depthRange[0]);

  constructor (
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) { }

  public ngOnInit () {
    const { queryParams } = this._activatedRoute.snapshot;

    this.searchInputControl.setValue(queryParams[SEARCH_INPUT_PARAM]);
    this.searchInputControl.valueChanges.pipe(
      debounceTime(200)
    ).subscribe(searchInput => this._router.navigate([], {
      queryParams: { [SEARCH_INPUT_PARAM]: searchInput || null },
      queryParamsHandling: 'merge'
    }));

    this.depthControl.setValue(+queryParams[SEARCH_DEPTH]);
    this.depthControl.valueChanges.pipe(
      debounceTime(200),
      map(depth => depth !== 1 ? depth : null)
    ).subscribe(depth => this._router.navigate([], {
      queryParams: { [SEARCH_DEPTH]: depth },
      queryParamsHandling: 'merge'
    }));
  }
}
