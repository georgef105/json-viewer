import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JsonNode, JsonService, FlatJsonNode } from '../json';
import { Observable } from 'rxjs';
import { SEARCH_INPUT_PARAM, SEARCH_DEPTH } from './search-header/search-header.component';
import { map } from 'rxjs/operators';
import { MAX_LIST_LENGTH } from '../tree-view/tree-view.component';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor (
    private _activatedRoute: ActivatedRoute,
    private _jsonService: JsonService
  ) { }

  public filterJson (fullObject: FlatJsonNode): Observable<Array<FlatJsonNode>> {
    return this._activatedRoute.queryParams.pipe(
      map(params => {
        return {
          searchInput: params[SEARCH_INPUT_PARAM] || '',
          depth: +params[SEARCH_DEPTH] || 1
        };
      }),
      map(({ searchInput, depth }) => {
        return (this._jsonService.getNodeChildren(fullObject) || [])
          .filter(child => this.hasMatch(child, searchInput, depth))
          .slice(0, MAX_LIST_LENGTH);
      })
    );
  }

  private hasMatch (flatNode: FlatJsonNode, searchInput: string, depth: number): boolean {
    return depth
      && flatNode.key.includes(searchInput)
      || flatNode.value && typeof flatNode.value === 'string' && flatNode.value.includes(searchInput)
      || (this._jsonService.getNodeChildren(flatNode) || [])
        .some(value => this.hasMatch(value, searchInput, depth - 1));
  }
}
