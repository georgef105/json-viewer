import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { JsonNode, JsonValue, FlatJsonNode } from './json-node.interface';
import { WebworkerService } from '../webworker/webworker.service';

@Injectable({
  providedIn: 'root'
})
export class JsonService {
  private _currentFile$ = new BehaviorSubject<JsonNode>({});
  private _isLoading$ = new Subject<boolean>();

  constructor(
    private _webworkerService: WebworkerService
  ) { }

  public pushFile (string: string): void {
    this._isLoading$.next(true);
    this._webworkerService.parseString(string)
      .then(response => {
        this._currentFile$.next(response);
        this._isLoading$.next(false);
      });
  }

  public getJsonFile (): Observable<JsonNode> {
    return this._currentFile$;
  }

  public getIsLoading (): Observable<boolean> {
    return this._isLoading$;
  }

  public getNodeChildren (node: FlatJsonNode, trimLength?: number): Array<FlatJsonNode> {
    if (this.isObject(node.value)) {
      const result = Object.keys(node.value).map(key => ({
        key,
        value: node.value && node.value[key],
        path: [...node.path, key]
      })).slice(0, trimLength);
      return result;
    }

    return null;
  }

  private isObject (node: JsonValue): node is JsonNode {
    return node && typeof node === 'object';
  }
}
