export interface JsonNode {
  [childKey: string]: JsonNode | Array<JsonNode> | string | number;
}

export type JsonValue = JsonNode | Array<JsonNode> | string | number;

export interface FlatJsonNode {
  key: string;
  value: JsonValue;
  path: Array<string>;
}
