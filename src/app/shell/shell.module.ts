import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { ShellComponent } from './shell.component';
import { RouterModule } from '@angular/router';
import { SearchHeaderModule } from '../search/search-header/search-header.module';

@NgModule({
  declarations: [
    ShellComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HeaderModule,
    SearchHeaderModule
  ],
  exports: [
    ShellComponent
  ]
})
export class ShellModule { }
