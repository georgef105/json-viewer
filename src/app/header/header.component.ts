import { Component, OnInit } from '@angular/core';
import { JsonService } from '../json';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public isLoading$: Observable<boolean>;
  constructor(
    private _jsonService: JsonService
  ) { }

  ngOnInit() {
    this.isLoading$ = this._jsonService.getIsLoading();
  }

}
