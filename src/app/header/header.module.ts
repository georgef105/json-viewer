import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FileUploaderModule } from '../file-uploader/file-uploader.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    FileUploaderModule,
    MatProgressBarModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
