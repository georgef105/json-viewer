import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreeViewRoutingModule } from './tree-view-routing.module';
import { TreeViewComponent } from './tree-view.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';

@NgModule({
  declarations: [
    TreeViewComponent
  ],
  imports: [
    CommonModule,
    TreeViewRoutingModule,

    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatTreeModule
  ],
  exports: [
    TreeViewComponent
  ]
})
export class TreeViewModule { }
