import { Component, OnInit } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { scan, map, switchMap, pluck } from 'rxjs/operators';
import { JsonService, FlatJsonNode } from '../json';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../search/search.service';

export interface Breadcrumb {
  path: string;
  displayName: string;
}

export const MAX_LIST_LENGTH = 200;

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss']
})
export class TreeViewComponent implements OnInit {
  public isLoading$: Observable<boolean>;

  public treeControl = new NestedTreeControl<FlatJsonNode>(node => this._jsonService.getNodeChildren(node, MAX_LIST_LENGTH));

  public dataSource$: Observable<MatTreeNestedDataSource<FlatJsonNode>>;

  public breadcrumbs$: Observable<Array<Breadcrumb>>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _jsonService: JsonService,
    private _searchService: SearchService
  ) { }

  ngOnInit() {
    this.isLoading$ = this._jsonService.getIsLoading();

    const pathSegments$: Observable<string[]> = this._activatedRoute.params.pipe(
      map(params => params.path.split('/').filter(Boolean))
    );

    this.breadcrumbs$ = pathSegments$.pipe(
      map(segments => {
        return ['...', ...segments].map((segment, index) => {
          const segmentDepth = (segments.length - index);
          const path = segmentDepth
            ? new Array(segmentDepth).fill('..').join('/')
            : './';
          return {
            path,
            displayName: segment
          };
        });
      })
    );

    this.dataSource$ = combineLatest(pathSegments$, this._jsonService.getJsonFile()).pipe(
      switchMap(([pathSegments, jsonData]) => {
        const data$ = pathSegments.length ? of(jsonData).pipe(
          pluck(...pathSegments)
        ) : of(jsonData);

        const [ key ] = pathSegments.reverse();
        return data$.pipe(
          map(value => ({
            key,
            value,
            path: []
          }))
        );
      }),
      switchMap((data: FlatJsonNode) => this._searchService.filterJson(data)),
      scan<FlatJsonNode[], MatTreeNestedDataSource<FlatJsonNode>> ((dataSource: MatTreeNestedDataSource<FlatJsonNode>, data) => {
        dataSource.data = data;
        return dataSource;
      }, new MatTreeNestedDataSource<FlatJsonNode>())
    );
  }

  public hasChild (_: number, node: FlatJsonNode) {
    return node.value && typeof node.value === 'object';
  }

}
