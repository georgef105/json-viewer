import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TreeViewComponent } from './tree-view.component';
import { treeMatcher } from './tree-view.route-matcher';

const routes: Routes = [
  {
    matcher: treeMatcher,
    component: TreeViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TreeViewRoutingModule { }
