import { UrlMatcher, UrlSegment, UrlMatchResult } from '@angular/router';

export function treeMatcher (segments: Array<UrlSegment>): UrlMatchResult {
  const path = segments.map(segment => segment.path).join('/');

  return {
    consumed: segments,
    posParams: {
      path: new UrlSegment(path, {})
    }
  };
}
