import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { JsonService } from '../json';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss']
})
export class FileUploaderComponent {
  public isLoading$: Observable<boolean>;

  constructor(
    private _jsonService: JsonService
  ) { }

  public fileChange (event): void {
    const { files } = event.target;

    if (files.length) {
      const reader = new FileReader();
      reader.onload = this.onFileUploaded(files[0]);

      reader.readAsText(files[0]);
    }
  }

  private onFileUploaded (file: File): any {
    return event => {
      const { result } = event.target;
      this._jsonService.pushFile(result);
    };
  }

}
